/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

// TODO Add helper header
//#include <cassert>
//#include <fstream>
#include <iostream>
//#include <string>
#include "ns3/ipv4-address.h"
#include "ns3/ipv4-header.h"
#include "ns3/ipv6-header.h"
#include "ns3/object.h"
#include "ns3/packet.h"
#include "ns3/ptr.h"
#include "ns3/core-module.h"
#include "ns3/packet-metadata.h"
#include "ns3/lisp-over-ipv4-impl.h"
#include "ns3/lisp-protocol.h"
#include "ns3/simple-map-tables.h"
#include "ns3/map-tables.h"

using namespace ns3;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("LispTest");


int
main (int argc, char *argv[])
{
  /*LispOverIpv4Impl lisp;
  Ptr<Packet> packet = Create<Packet>();
  Ptr<Packet> packet2 = Create<Packet> ();
  Ipv4Header ipv4Header = Ipv4Header ();
  Ipv6Header ipvAuxHeader = Ipv6Header ();

  ipv4Header.SetSource(Ipv4Address ("10.0.0.2"));
  ipv4Header.SetDestination(Ipv4Address ("10.0.0.3"));

 packet2->EnableChecking();
  packet->AddPaddingAtEnd(10);

  packet->AddHeader(ipv4Header);
  packet2->AddHeader(ipvAuxHeader);



  PacketMetadata::Item item;
  PacketMetadata::ItemIterator metadataIterator = packet2->BeginItem();

  while (metadataIterator.HasNext())
    {
      item = metadataIterator.Next();
      if (item.tid.GetName() == Ipv6Header::GetTypeId().GetName ())
        NS_LOG_LOGIC("OKKKKK " << Ipv6Header::GetTypeId().GetName ());
    }

  cout << packet->GetSize() << " " << packet2->GetSize() << "\n";*/

  NS_LOG_LOGIC("trying simple map tables");

  Ptr<EndpointId> eid = Create<EndpointId> (static_cast<Address> (Ipv4Address ("10.0.0.1")), Ipv4Mask ("255.255.255.0"));
  Ptr<EndpointId> eid1 = Create<EndpointId> (static_cast<Address> (Ipv4Address ("10.0.0.1")), Ipv4Mask ("255.255.255.0"));

  SimpleMapTables mapTables = SimpleMapTables ();

  Ptr<MapEntryImpl> mapEntry = Create<MapEntryImpl> ();

  mapEntry->InsertLocator(Create<Locator> (static_cast<Address> (Ipv4Address ("10.0.0.3"))));

  mapTables.SetEntry(static_cast<Address> (Ipv4Address ("10.0.0.1")), Ipv4Mask ("255.255.255.0"), mapEntry, MapTables::IN_DATABASE);

  Ptr<MapEntry> entry = mapTables.DatabaseLookup (static_cast<Address> (Ipv4Address ("10.0.0.2")));

  Ptr<Locator> locator = entry->FindLocator (static_cast<Address> (Ipv4Address ("10.0.0.3")));
  if (entry != 0)
    NS_LOG_LOGIC (entry->Print ());
  else NS_LOG_LOGIC ("OOPS");
  //NS_LOG_LOGIC(mapTables.GetNMapEntries () + " " + eid->Print ());
  NS_LOG_LOGIC(locator->Print ());

  return 0;
}
