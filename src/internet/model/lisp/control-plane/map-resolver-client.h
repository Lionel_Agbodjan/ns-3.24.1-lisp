/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 University of Liege
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lionel Agbodjan <lionel.agbodjan@gmail.com>
 */

#ifndef MAP_RESOLVER_CLIENT_H_
#define MAP_RESOLVER_CLIENT_H_

#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/traced-callback.h"

namespace ns3
{
class Socket;
class Packet;
class LispMappingSocket;

class MapResolverClient : public Application
{
public:
  MapResolverClient ();
  virtual
  ~MapResolverClient ();

  static TypeId
  GetTypeId (void);

protected:
  virtual void DoDispose (void);

private:
  virtual void StartApplication (void);

  virtual void StopApplication (void);

  /**
   * \brief Schedule the next packet transmission
   * \param dt time interval between packets.
   */
  void ScheduleTransmit (Time dt);

  /**
   * \brief Send a packet
   */
  void Send (void);

  void SendToLisp (void);

  void HandleMapSockRead (Ptr<Socket> lispMappingSocket);

  /**
   * \brief Handle a packet reception.
   *
   * This function is called by lower layers.
   *
   * \param socket the socket the packet was received to.
   */
  void HandleRead (Ptr<Socket> socket);

  Ptr<Socket> m_lispMappingSocket;
  uint32_t m_sent;
  uint32_t m_count;
  Time m_interval; //!< Packet inter-send time
  Address m_lispProtoAddress;
  EventId m_event;

};

} /* namespace ns3 */
#endif /* MAP_RESOLVER_CLIENT_H_ */
