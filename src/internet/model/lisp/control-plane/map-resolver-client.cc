/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 University of Liege
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lionel Agbodjan <lionel.agbodjan@gmail.com>
 */
#include "map-resolver-client.h"

// TODO remove some
#include "ns3/log.h"
#include "ns3/address-utils.h"
#include "ns3/nstime.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "ns3/lisp-mapping-socket.h"

/*
 *
 */
namespace ns3
{

NS_LOG_COMPONENT_DEFINE ("MapResolverClientApplication");

NS_OBJECT_ENSURE_REGISTERED (MapResolverClient);

TypeId
MapResolverClient::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::MapResolverClient")
    .SetParent<Application> ()
    .SetGroupName ("Lisp")
    .AddConstructor<MapResolverClient> ()
    .AddAttribute ("MaxPackets",
                           "The maximum number of packets the application will send",
                           UintegerValue (100),
                           MakeUintegerAccessor (&MapResolverClient::m_count),
                           MakeUintegerChecker<uint32_t> ())
    .AddAttribute (
                "Interval", "The time to wait between packets",
                TimeValue (Seconds (1.0)),
                MakeTimeAccessor (&MapResolverClient::m_interval), MakeTimeChecker ())
  ;
  return tid;
}


MapResolverClient::MapResolverClient ()
{
  NS_LOG_FUNCTION (this);
  m_lispMappingSocket = 0;
  m_event = EventId ();
  m_sent = 0;
  m_lispProtoAddress = Address (); // invalid address
  m_count = 100;
  m_interval = Seconds (1.0);
  NS_LOG_DEBUG ("Application created");
}

MapResolverClient::~MapResolverClient ()
{
  NS_LOG_FUNCTION (this);
  m_lispMappingSocket = 0;
}

void MapResolverClient::StartApplication (void)
{
  if (m_lispMappingSocket == 0)
    {
      NS_LOG_DEBUG ("Trying to Connect to lispProto");
      TypeId tid = TypeId::LookupByName("ns3::LispMappingSocketFactory");
      m_lispMappingSocket = Socket::CreateSocket (GetNode (), tid);
      Ptr<LispOverIp> lisp = m_lispMappingSocket->GetNode ()->GetObject<LispOverIp> ();
      m_lispProtoAddress = lisp->GetLispMapSockAddress ();
      m_lispMappingSocket->Bind ();
      m_lispMappingSocket->Connect (m_lispProtoAddress);
      NS_LOG_DEBUG ("Connected to " << m_lispProtoAddress);
    }
  m_lispMappingSocket->SetRecvCallback (MakeCallback (&MapResolverClient::HandleMapSockRead, this));

  ScheduleTransmit (Seconds (0.));
}

void MapResolverClient::StopApplication (void)
{
  NS_LOG_FUNCTION (this);

  if (m_lispMappingSocket != 0)
    {
      m_lispMappingSocket->Close ();
      m_lispMappingSocket->SetRecvCallback (
          MakeNullCallback<void, Ptr<Socket> > ());
      m_lispMappingSocket = 0;
    }

  Simulator::Cancel (m_event);
}

void MapResolverClient::ScheduleTransmit (Time dt)
{
  NS_LOG_FUNCTION (this << dt);
   m_event = Simulator::Schedule (dt, &MapResolverClient::SendToLisp, this);
}

void MapResolverClient::Send (void)
{

}

void MapResolverClient::SendToLisp (void)
{
  NS_LOG_FUNCTION (this);

   NS_ASSERT (m_event.IsExpired ());

   Ptr<Packet> p;

   p = Create<Packet> (reinterpret_cast<const uint8_t*> ("hello"), 5);

   m_lispMappingSocket->Send (p);

   ++m_sent;

   if (m_sent < m_count)
     {
       ScheduleTransmit (m_interval);
     }
   NS_LOG_DEBUG ("Hey I sent something!");
}


void MapResolverClient::HandleRead (Ptr<Socket> socket)
{

}

void MapResolverClient::HandleMapSockRead (Ptr<Socket> lispMappingSocket)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG ("Hello I got something :)");
}

void MapResolverClient::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  Application::DoDispose ();
}

} /* namespace ns3 */
