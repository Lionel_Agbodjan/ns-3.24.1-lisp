/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 University of Liege
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lionel Agbodjan <lionel.agbodjan@gmail.com>
 */
#include "map-server-etr-client.h"

// TODO remove some
#include "ns3/log.h"
#include "ns3/address-utils.h"
#include "ns3/nstime.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"

namespace ns3
{

NS_LOG_COMPONENT_DEFINE ("MapServerEtrClient");

NS_OBJECT_ENSURE_REGISTERED (MapServerEtrClient);
TypeId
MapServerEtrClient::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::MapServerEtrClient")
    .SetParent<Application> ()
    .SetGroupName ("Lisp")
    .AddConstructor<MapServerEtrClient> ()
  ;
  return tid;
}

MapServerEtrClient::MapServerEtrClient ()
{
  // TODO Auto-generated constructor stub

}

MapServerEtrClient::~MapServerEtrClient ()
{
   // TODO Auto-generated destructor stub
}

void
MapServerEtrClient::StartApplication (void)
{

}

void MapServerEtrClient::StopApplication (void)
{

}

void MapServerEtrClient::ScheduleTransmit (Time dt)
{

}

void MapServerEtrClient::Send (void)
{

}

void MapServerEtrClient::SendToLisp (void)
{

}

void MapServerEtrClient::HandleRead (Ptr<Socket> socket)
{

}

} /* namespace ns3 */
