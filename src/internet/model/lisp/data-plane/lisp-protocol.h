/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 University of Liege
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lionel Agbodjan <lionel.agbodjan@gmail.com>
 */
#ifndef LISP_H_
#define LISP_H_

#include "ns3/address.h"
#include "ns3/log.h"
#include "ns3/packet.h"
#include "ns3/ptr.h"
#include "ns3/simple-ref-count.h"
#include "lisp-header.h"

namespace ns3
{
  class TypeId;
} /* namespace ns3 */

namespace ns3 {

class Packet;
class Address;
class MapEntry;
class Locator;
class LispOverIp;

class LispProtocol
{

public:
  static const uint8_t PROT_NUMBER; //!< protocol number (0x)
  static const uint16_t LISP_DATA_PORT;
  static const uint16_t LISP_SIG_PORT;
  static const uint16_t LISP_MAX_RLOC_PRIO;
  static const uint16_t MAX_VERSION_NUM;
  static const uint16_t WRAP_VERSION_NUM;
  /*
   * The following constant cannot be used as a Map-Version number.
   * It indicates that no Map-Version number is assigned to the
   * EID-TO-RLOC mapping
   */
  static const uint8_t NULL_VERSION_NUM;

  // TODO set number of RLOC in the mapping database/cache

  enum EtrState
    {
      LISP_ETR_STANDARD = 1,
      LISP_ETR_NOTIFY = 2,
      LISP_ETR_SECURE = 3
    };
  /**
   *
   * @return
   */
  static TypeId GetTypeId (void);


  LispProtocol ();
  virtual ~LispProtocol ();

  static Ptr<Packet> PrependLispHeader (Ptr<Packet> packet, Ptr<const MapEntry>
  localMapEntry, Ptr<const MapEntry> remoteMapEntry, Ptr<Locator> sourceRloc,
  Ptr<Locator> destRloc);

  static bool CheckLispHeader (const LispHeader &header, Ptr<const MapEntry>
  localMapEntry, Ptr<const MapEntry> remoteMapEntry, Ptr<Locator> srcRloc,
  Ptr<Locator> destRloc, Ptr<LispOverIp> lispOverIp);

  static uint16_t GetLispSrcPort (Ptr<const Packet> packet);

   // true if vnum2 newer (i.e., greater) than vnum1

  /**
   * To determine if V2 is greater (newer) than V1:
   * 1. V1 = V2 : The Map-Version numbers are the same.
   * 2. V2 > V1 : if and only if
   * V2 > V1 AND (V2 - V1) <= 2**(N-1)
   * OR
   * V1 > V2 AND (V1 - V2) > 2**(N-1)
   * 3. V1 > V2 : otherwise
   *
   * @param vnum1
   * @param vnum2
   * @return
   */
  static bool IsMapVersionNumberNewer (uint16_t vnum2, uint16_t vnum1);
};

class LispStatistics : public Object
{
public:

  // TODO Add attributes
  static TypeId GetTypeId (void);
  LispStatistics ();
  ~LispStatistics ();

  void NoLocalMap (void);
  void BadDestVersionNumber (void);
  void BadSrcVersionNumber (void);
  void IncInputPacket (void);
  void IncBadSizePackets (void);
  void IncInputDifAfPackets (void);
  void IncCacheMissPackets (void);
  void IncNoValidRloc (void);
  void IncOutputDropPackets (void);
  void IncNoValidMtuPackets (void);
  void IncNoEnoughSpace (void);
  void IncOutputPackets (void);
  void IncOutputDifAfPackets (void);

private:
  // input statistics
  /*
   * total input packets
   */
  uint32_t m_inputPackets;
  /*
   * total input packets with a different
   * address family in the outer header packet
   */
  uint32_t m_inputDifAfPackets;
  /*
   * packets dropped because they are shorter
   * that their header.
   */
  uint32_t m_badSizePackets; // TODO may be remove
  /*
   * packets for which there is no
   * local mapping
   */
  uint32_t m_noLocMapPresent;
  /*
   * packets that are dropped because
   * the data length is larger that the packets
   * themselves
   */
  uint32_t m_badDataLength; // TODO may be remove
  /*
   * Packets with bad source version number
   */
  uint32_t m_badSourceVersionNumber;
  /*
   * packets with a bad destination version number
   */
  uint32_t m_badDestVesionNumber;
  // output statistics
  /*
   * total number of output lisp packets
   */
  uint32_t m_outputPackets;
  /*
   * total number of input packets with a different
   * address family in the inner header
   */
  uint32_t m_outputDifAfPackets;
  /*
   * packets that are dropped because of a cache miss
   */
  uint32_t m_cacheMissPackets;
  /*
   * packets dropped because there is no valid RLOC
   */
  uint32_t m_noValidRlocPackets;
  /*
   * packets dropped because they
   * don't pass the MTU check
   */
  uint32_t m_noValidMtuPackets;
  /*
   * packets dropped because the there is no buffer space
   */
  uint32_t m_noEnoughBufferPacket;
  /*
   * total output packets that are dropped
   */
  uint32_t m_outputDropPackets;
};

class RlocMetrics : public SimpleRefCount<RlocMetrics>
{
public:

  //static TypeId GetTypeId (void);
  RlocMetrics ();
  RlocMetrics (uint8_t priority, uint8_t weight);
  RlocMetrics (uint8_t priority, uint8_t weight, bool reachable);
  ~RlocMetrics ();

  enum RlocMetricsFlags {
    RLOCF_UP = 0x01,
    RLOCF_LIF = 0x02,
    RLOC_TXNONCE = 0x04,
    RLOCF_RXNONCE = 0x08,
  };
  /**
   *
   * @return
   */
  uint8_t GetPriority (void) const;
  void SetPriority (uint8_t priority);
  uint8_t GetWeight (void) const;
  void SetWeight (uint8_t weight);
  uint32_t GetMtu (void) const;
  void SetMtu (uint32_t mtu);

  bool IsUp (void);
  void SetUp (bool status);

  bool IsLocalInterface (void);
  void SetIsLocalIf (void);

  bool IsTxNoncePresent (void);
  void SetTxNoncePresent (bool txNoncePresent);

  bool IsRxNoncePresent (void);
  void SetRxNoncePresent (bool rxNoncePresent);

  uint32_t GetTxNonce (void);
  void SetTxNonce (uint32_t txNonce);

  uint32_t GetRxNonce (void);
  void SetRxNonce (uint32_t rxNonce);

  std::string Print ();
  uint8_t Serialize (uint8_t *buf);
  static Ptr<RlocMetrics> Deserialized (uint8_t *buf);

private:
  uint8_t m_priority; // Rloc priority
  /*
   * locator weight: used when several Rlocs have the same
   * priority. The sum of the weight fields of the Rlocs having the
   * same priority must always be 100 or 0.
   */
  uint8_t m_weight;
  bool m_rlocIsUp; // Rloc Status Bit
  bool m_rlocIsLocalInterface; // Rloc is a local interface
  bool m_txNoncePresent; // Rloc Tx Nonce is present
  bool m_rxNoncePresent; // Rloc Rx Nonce is present
  uint32_t m_txNonce; // used when sending a LISP encapsulated packet
  uint32_t m_rxNonce; // used when receiving LISP encapsulated packet
  /*
   * This is useful for local mapping for which flag 'i' is
   * set.
   * Initialized at creation by copying the mtu of the corresponding
   * interface here. This value is not updated if changed. (TODO OK ?)
   */
  uint32_t m_mtu;
};

/**
 *
 */
class Locator : public SimpleRefCount<Locator>
{
public:

  Locator (Address rlocAddress);
  Locator (); // todo remove this later
  ~Locator ();
  Address GetRlocAddress (void) const;
  void SetRlocAddress (const Address &rlocAddress);
  Ptr<RlocMetrics> GetRlocMetrics (void) const;
  void SetRlocMetrics (Ptr<RlocMetrics> rlocMetrics);
  std::string Print (void);

  uint8_t Serialize (uint8_t *buf);
  static Ptr<Locator> Deserialized (uint8_t *buf);

private:
  Address m_rlocAddress;
  Ptr<RlocMetrics> m_metrics;
};

/**
 *
 */
class EndpointId : public SimpleRefCount<EndpointId> {
public:
  EndpointId ();
  EndpointId (const Address &eidAddress);
  EndpointId (const Address &eidAddress, const Ipv4Mask &mask);
  EndpointId (const Address &eidAddress, const Ipv6Prefix &prefix);
  ~EndpointId ();
  Address GetEidAddress (void) const;
  void SetEidAddress (const Address &eidAddress);
  Ipv4Mask GetIpv4Mask () const;
  void SetIpv4Mask (const Ipv4Mask &mask);
  void SetIpv6Prefix (const Ipv6Prefix &prefix);
  Ipv6Prefix GetIpv6Prefix (void) const;
  bool IsIpv4 (void) const;
  std::string Print (void) const;

  uint8_t Serialize (uint8_t buf[33]);

  static Ptr<EndpointId> Deserialize (const uint8_t *buf);
private:
  Address m_eidAddress;
  Ipv4Mask m_mask;
  Ipv6Prefix m_prefix;
};

} /* namespace ns3 */

#endif /* LISP_H_ */
