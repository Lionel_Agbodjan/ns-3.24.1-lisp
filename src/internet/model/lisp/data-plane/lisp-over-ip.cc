/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 University of Liege
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lionel Agbodjan <lionel.agbodjan@gmail.com>
 */

#include <stdint.h>
#include "lisp-over-ip.h"
#include "ns3/node.h"
#include <ns3/log.h>
#include "ns3/object.h"
#include "ns3/object-vector.h"
#include "lisp-mapping-socket-factory.h"
#include "lisp-mapping-socket.h"

namespace ns3
{

NS_LOG_COMPONENT_DEFINE ("LispOverIp");

NS_OBJECT_ENSURE_REGISTERED (LispOverIp);

TypeId
LispOverIp::GetTypeId ()
{
static TypeId tid = TypeId ("ns3::LispOverIp")
    .SetParent<Object> ()
    .SetGroupName ("Lisp")
    .AddAttribute ("SocketList", "The list of sockets associated to this protocol (lisp).",
                    ObjectVectorValue (),
                    MakeObjectVectorAccessor (&LispOverIp::m_sockets),
                    MakeObjectVectorChecker<LispMappingSocket> ())
  ;
  return tid;
}

LispOverIp::LispOverIp (Ptr<LispStatistics> statisticsForIpv4, Ptr<LispStatistics> statisticsForIpv6)
{
  NS_LOG_FUNCTION (this);
  NS_ASSERT (statisticsForIpv4 && statisticsForIpv6);
  m_statisticsForIpv4 = statisticsForIpv4;
  m_statisticsForIpv6 = statisticsForIpv6;
}

void LispOverIp::SetLispStatistics (Ptr<LispStatistics> statisticsV4, Ptr<LispStatistics> statisticsV6)
{
  m_statisticsForIpv4 = statisticsV4;
  m_statisticsForIpv6 = statisticsV6;
}

  LispOverIp::LispOverIp ()
  {
    NS_LOG_FUNCTION (this);
    m_lispSocket = CreateSocket ();
  }

  LispOverIp::~LispOverIp ()
  {
    NS_LOG_FUNCTION (this);
  }

Ptr<Node> LispOverIp::GetNode ()
{
  return m_node;
}

void LispOverIp::SetNode (Ptr<Node> node)
{
  m_node = node;
}

void
LispOverIp::NotifyNewAggregate ()
{
  NS_LOG_FUNCTION (this);
  if (m_node == 0)
    {
      Ptr<Node>node = this->GetObject<Node>();
      // verify that it's a valid node and that
      // the node has not been set before
      if (node != 0)
        {
          this->SetNode (node);
          if (!node->GetObject<LispMappingSocketFactory> ())
            {
              Ptr<LispMappingSocketFactory> mappingFactory = CreateObject<LispMappingSocketFactory> ();
              mappingFactory->SetLisp (this);
              node->AggregateObject(mappingFactory);
            }
        }
    }
  Object::NotifyNewAggregate ();
}

void LispOverIp::DoDispose (void)
{

}

Ptr<Socket> LispOverIp::CreateSocket (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  Ptr<LispMappingSocket> socket = CreateObject<LispMappingSocket> ();
    socket->SetNode (m_node);
    socket->SetLisp (this);
    socket->SetSockIndex (m_sockets.size ());
    m_sockets.push_back (socket);
    return socket;
}

Ptr<LispMappingSocket> LispOverIp::GetMappingSocket (uint8_t sockIndex)
{
  return m_sockets.at (sockIndex);
}

void LispOverIp::SetMapTablesIpv4 (Ptr<MapTables> mapTablesIpv4)
{
  NS_ASSERT (mapTablesIpv4 != 0);
  m_mapTablesIpv4 = mapTablesIpv4;
  m_mapTablesIpv4->SetLispOverIp (this);
}
void LispOverIp::SetMapTablesIpv6 (Ptr<MapTables> mapTablesIpv6)
{
  NS_ASSERT (mapTablesIpv6 != 0);
  m_mapTablesIpv6 = mapTablesIpv6;
  m_mapTablesIpv6->SetLispOverIp (this);
}

Ptr<MapEntry> LispOverIp::DatabaseLookup (Address const &eidAddress) const
{
  if (Ipv4Address::IsMatchingType(eidAddress)) {
      return m_mapTablesIpv4->DatabaseLookup (eidAddress);
  }
  else if (Ipv6Address::IsMatchingType (eidAddress)) {
      return m_mapTablesIpv6->DatabaseLookup (eidAddress);
  }
  return 0;
}

Ptr<MapEntry> LispOverIp::CacheLookup (Address const &eidAddress) const
{
  if (Ipv4Address::IsMatchingType(eidAddress)) {
      return m_mapTablesIpv4->CacheLookup (eidAddress);
  }
  else if (Ipv6Address::IsMatchingType (eidAddress)) {
      return m_mapTablesIpv6->CacheLookup (eidAddress);
  }
  return 0;
}

Ptr<Locator> LispOverIp::SelectDestinationRloc(Ptr<const MapEntry> mapEntry) const
{
  return mapEntry->RlocSelection ();
}

Ptr<Locator> LispOverIp::SelectSourceRloc(Address const &srcEid, Ptr<const Locator> destLocator) const
{
  if (Ipv4Address::IsMatchingType (srcEid))
    {
      return m_mapTablesIpv4->SourceRlocSelection(srcEid, destLocator);
    }
  else if (Ipv6Address::IsMatchingType (srcEid))
    {
      return m_mapTablesIpv6->SourceRlocSelection (srcEid, destLocator);
    }
  return 0;
}

Ptr<LispStatistics> LispOverIp::GetLispStatisticsV4 (void)
{
  return m_statisticsForIpv4;
}

Ptr<LispStatistics> LispOverIp::GetLispStatisticsV6 (void)
{
  return m_statisticsForIpv6;
}

void LispOverIp::OpenLispMappingSocket (void)
{
  NS_LOG_FUNCTION (this);
  // create mapping socket address
  m_lispAddress = static_cast<Address> (MappingSocketAddress (m_node->GetDevice(0)->GetAddress (), 0));

  // bind
  m_lispSocket->Bind (m_lispAddress);
  m_lispSocket->SetRecvCallback (MakeCallback (&LispOverIp::HandleMapSockRead, this));
  NS_LOG_DEBUG ("Bound to " << m_lispAddress);
}

void LispOverIp::HandleMapSockRead (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this);

  Ptr<Packet> packet;
  Address from;

  while ((packet = socket->RecvFrom (from)))
    {
      uint8_t *buf = new uint8_t [packet->GetSize ()];
      packet->CopyData (buf, packet->GetSize ());
      NS_LOG_DEBUG ("Hello I got something :)!" << (reinterpret_cast<char *> (buf)));
    }

}

Address LispOverIp::GetLispMapSockAddress (void)
{
  return m_lispAddress;
}

} /* namespace ns3 */
