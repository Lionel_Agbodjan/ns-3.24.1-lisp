/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015-2016 University of Liege
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author:
 */

#ifndef LISP_HEADER_H
#define LISP_HEADER_H

#include "ns3/header.h"
#include "ns3/object.h"

namespace ns3 {

/**
 * \class LispHeader
 * \brief Packet header for LISP
 */
class LispHeader : public Header
{
public:

  /**
   * \brief Get the type identifier.
   * \return type identifier
   */
  static TypeId GetTypeId (void);

  /**
   * \brief Return the instance type identifier.
   * \return instance type ID
   */
  virtual TypeId GetInstanceTypeId (void) const;

  /**
   * \brief Constructor.
   */
  LispHeader (void);


  /**
   *
   */
  void SetNBit (uint8_t N);

  /**
   *
   */
  uint8_t GetNBit (void) const;

  /**
   *
   */
  void SetLBit (uint8_t N);

  /**
   *
   */
  uint8_t GetLBit (void) const;

  /**
   *
   */
  void SetEBit (uint8_t N);

  /**
   *
   */
  uint8_t GetEBit (void) const;

  /**
   *
   */
  void SetVBit (uint8_t N);

  /**
   *
   */
  uint8_t GetVBit (void) const;

  /**
   *
   */
  void SetIBit (uint8_t N);

  /**
   *
   */
  uint8_t GetIBit (void) const;

  /**
   *
   */
  void SetFlagsBits (uint8_t N);

  /**
   *
   */
  uint8_t GetFlagsBits (void) const;

  /**
   *
   */
  void SetNonce (uint32_t N);

  /**
   *
   */
  uint32_t GetNonce (void) const;

  /**
   *
   */
  void SetLSBs (uint32_t N);

  /**
   *
   */
  uint32_t GetLSBs (void) const;

  /**
   *
   */
  void SetSrcMapVersion (uint32_t N);

  /**
   *
   */
  uint32_t GetSrcMapVersion (void) const;

  /**
   *
   */
  void SetDestMapVersion (uint32_t N);

  /**
   *
   */
  uint32_t GetDestMapVersion (void) const;

  /**
   *
   */
  void SetInstanceID (uint32_t N);

  /**
   *
   */
  uint32_t GetInstanceID (void) const;
  /**
   * \brief Print some informations about the packet.
   * \param os output stream
   * \return info about this packet
   */
  virtual void Print (std::ostream& os) const;

  /**
   * \brief Get the serialized size of the packet.
   * \return size
   */
  virtual uint32_t GetSerializedSize (void) const;

  /**
   * \brief Serialize the packet.
   * \param start Buffer iterator
   */
  virtual void Serialize (Buffer::Iterator start) const;

  /**
   * \brief Deserialize the packet.
   * \param start Buffer iterator
   * \return size of the packet
   */
  virtual uint32_t Deserialize (Buffer::Iterator start);

private:
  /**
   * \brief The N-bit (Nonce-present bit).
   */
  uint8_t m_N : 1;

  /**
   * \brief The L-bit (the 'Locator-Status-Bits' field enabled bit)
   */
  uint8_t  m_L : 1;

  /**
   * \brief The E-bit (echo-Nonce-request bit)
   */
   uint8_t m_E : 1;

   /**
    * \brief The V-bit (Map-Version present bit)
    * Note: The N-Bit and the V-Bit cannot be set
    * simultaneously.
    */
   uint8_t m_V : 1;

   /**
    * \brief The I-bit (Instance ID bit)
    */
   uint8_t m_I : 1;

   /**
    *
    */
   uint8_t m_flags : 3;

  /**
   *
   */
  uint32_t m_nonce;

  /**
   * \brief The LISP Locator-Status-Bits (LSBs).
   * \note 32 bits when I == 0 and 8 otherwise
   */
  uint32_t m_lsbs;

  /**
   * \brief
   */
  uint16_t m_sourceMapVersion; // 12 bist
  uint16_t m_destMapVersion; // 12 bits

  /**
   * \brief 24
   */
  uint32_t m_instanceId; // 24 bits

};

} /* namespace ns3 */

#endif /* LISP_HEADER_H */

