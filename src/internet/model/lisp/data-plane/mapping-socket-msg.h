/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 University of Liege
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lionel Agbodjan <lionel.agbodjan@gmail.com>
 */
#ifndef MAP_SOCKET_MSG_HEADER_H_
#define MAP_SOCKET_MSG_HEADER_H_

#include "ns3/header.h"
#include "ns3/object.h"

namespace ns3
{

class Address;
class Ipv6Prefix;
class Ipv4Mask;
class Locators;
class EndpointId;

class MappingSocketMsg
{
public:
  static TypeId GetTypeId (void);

  MappingSocketMsg ();
  virtual ~MappingSocketMsg ();

  void Print (std::ostream& os);

  Address GetEidAddress (void);
  Ipv6Prefix GetIpv6Prefix (void);
  Ipv4Mask GetMask (void);

  Ptr<Locators> GetLocators (void);

private:
  Ptr<EndpointId> endPoint; //!< prefix + netmask
  Ptr<Locators> locatorsList; //!< locators associated to the endpoint

};

class MappingSocketMsgHeader : public Header
{
public:
    /**
     * \brief Get the type identifier.
     * \return type identifier
     */
    static TypeId GetTypeId (void);

    MappingSocketMsgHeader ();
    virtual
    ~MappingSocketMsgHeader ();

    /**
     * \brief Print some informations about the packet.
     * \param os output stream
     * \return info about this packet
     */
    virtual void Print (std::ostream& os) const;

    /**
     * \brief Get the serialized size of the packet.
     * \return size
     */
    virtual uint32_t GetSerializedSize (void) const;

    /**
     * \brief Serialize the packet.
     * \param start Buffer iterator
     */
    virtual void Serialize (Buffer::Iterator start) const;

    /**
     * \brief Deserialize the packet.
     * \param start Buffer iterator
     * \return size of the packet
     */
    virtual uint32_t Deserialize (Buffer::Iterator start);

    /**
     * \brief Return the instance type identifier.
     * \return instance type ID
     */
    virtual TypeId GetInstanceTypeId (void) const;

  private:
    uint8_t m_mapVersion;
    uint16_t m_mapType;
    uint32_t m_mapFlags;
    uint32_t m_mapAddresses;
    uint16_t m_mapVersioning;
    uint32_t m_mapRlocCount;

  };

} /* namespace ns3 */

#endif /* MAP_SOCKET_MSG_H_ */
