/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 University of Liege
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lionel Agbodjan <lionel.agbodjan@gmail.com>
 */
#ifndef LISP_OVER_IP_H_
#define LISP_OVER_IP_H_

#include "ns3/node.h"
#include "ns3/packet.h"
#include "ns3/ptr.h"
#include "lisp-protocol.h"
#include "map-tables.h"
#include "ns3/object.h"
#include "mapping-socket-address.h"

namespace ns3
{


class MapTables;
class MapEntry;
class Node;
class Socket;
class LispMappingSocket;

class LispOverIp : public Object
{
public:
  static TypeId GetTypeId (void);
  LispOverIp ();
  LispOverIp (Ptr<LispStatistics> statisticsForIpv4, Ptr<LispStatistics> statisticsForIpv6);
  virtual
  ~LispOverIp ();

  Ptr<Node> GetNode (void);
  void SetNode (Ptr<Node>);

  Ptr<Socket> CreateSocket (void);

  void SetMapTablesIpv4 (Ptr<MapTables> mapTablesIpv4);
  void SetMapTablesIpv6 (Ptr<MapTables> mapTablesIpv6);
  Ptr<MapEntry> DatabaseLookup (Address const &eidAddress) const;
  Ptr<MapEntry> CacheLookup (Address const &eidAddress) const;

  Ptr<Locator> SelectDestinationRloc(Ptr<const MapEntry> mapEntry) const;
  Ptr<Locator> SelectSourceRloc(Address const &srcEid, Ptr<const Locator> destLocator) const;

  virtual Ptr<Packet> LispEncapsulate (Ptr<Packet> packet, uint16_t udpLength, uint16_t udpSrcPort) = 0;
  Ptr<LispStatistics> GetLispStatisticsV4 (void);
  Ptr<LispStatistics> GetLispStatisticsV6 (void);
  void SetLispStatistics (Ptr<LispStatistics> statisticsV4, Ptr<LispStatistics> statisticsV6);

  Ptr<LispMappingSocket> GetMappingSocket (uint8_t sockIndex);
  Address GetLispMapSockAddress (void);

  void OpenLispMappingSocket (void);
  void HandleMapSockRead (Ptr<Socket> socket);

protected:

  // Note: Each entry of the table can contain Ipv6 or Ipv4 RLOC addresses
  Ptr<MapTables> m_mapTablesIpv4;       //!< Map table for Ipv4 EID prefixes
  Ptr<MapTables> m_mapTablesIpv6;       //!< Map table for Ipv6 EID prefixes
  Ptr<LispStatistics> m_statisticsForIpv4;
  Ptr<LispStatistics> m_statisticsForIpv6;


private:

  /**
   * This function will notify other components connected to the node that a new stack member is now connected
   * This will be used to notify Layer 3 protocol of layer 4 protocol stack to connect them together.
   */
  virtual void NotifyNewAggregate ();
  virtual void DoDispose (void);
  std::vector<Ptr<LispMappingSocket> > m_sockets;       //!< list of mapping sockets
  Ptr<Socket> m_lispSocket;
  Address m_lispAddress;
  Ptr<Node> m_node;
};

} /* namespace ns3 */

#endif /* LISP_OVER_IP_H_ */
