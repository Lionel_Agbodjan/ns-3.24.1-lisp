/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 University of Liege
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lionel Agbodjan <lionel.agbodjan@gmail.com>
 */
#ifndef LISP_MAPPING_SOCKET_H_
#define LISP_MAPPING_SOCKET_H_

#include <stdint.h>
#include <queue>
#include "ns3/socket.h"
#include "ns3/ptr.h"
#include "lisp-over-ip.h"

namespace ns3
{
class Node;
class Packet;

class LispMappingSocket : public Socket
{
public:

  static TypeId GetTypeId (void);
  LispMappingSocket ();
  virtual
  ~LispMappingSocket ();

  // TODO explain each
  enum MapSockMsgType {
    MAPM_ADD = 1,
    MAPM_DELETE,
    MAPM_CHANGE,
    MAPM_GET,
    MAPM_MISS,
    MAPM_MISS_EID,
    MAPM_MISS_HEADER,
    MAPM_MISS_PACKET,
    MAPM_LSBITS,
    MAPM_LOCALSTALE,
    MAPM_REMOTESTALE,
    MAPM_NONCEMISMATCH
  };

  enum MappingFlags {
    MAPF_DB = 0x001,    // Mapping is part of Database
    MAPF_VERSIONING = 0x002,    // Mapping uses versioning
    MAPF_LOCBITS = 0x004,       // Mapping uses LocStatus bits
    MAPF_STATIC = 0x008,        // Mapping has been added manually
    MAPF_UP = 0x010,    // mapping is usable
    MAPF_ALL = 0x020,   // Operation concerns DB and Cache
    MAPF_EXPIRED = 0x040,       // TODO Add description
    MAPF_NEGATIVE = 0x080,      // Negative Mapping (no Rlocs forward natively)
    MAPF_DONE = 0x100,  // message confirmed (send back by the kernel to notify)
  };

  enum MissMsgType {
    LISP_MISSMSG_EID = 1,
    LISP_MISSMSG_HEADER,
    LISP_MISSMSG_PACKET,
  };

  enum PresentMapAddrs {
    MAPA_EID = 0x1,
    MAPA_EIDMASK = 0x2,
    MAPA_RLOC = 0x4
  };

  /**
   * \brief Set the node associated with this socket.
   * \param node node to set
   */
  void SetNode (Ptr<Node> node);

  void SetLisp (Ptr<LispOverIp> lisp);

  virtual enum Socket::SocketErrno GetErrno () const;

  /**
   * \brief Get socket type (NS3_SOCK_RAW)
   * \return socket type
   */
  virtual enum Socket::SocketType GetSocketType (void) const;

  virtual Ptr<Node> GetNode (void) const;
  virtual int Bind (const Address &address);
  virtual int Bind (void);
  virtual int Bind6 (void);
  virtual int GetSockName (Address &address) const;
  virtual int Close (void);
  virtual int ShutdownSend (void);
  virtual int ShutdownRecv (void);
  virtual int Connect (const Address &address);
  virtual int Listen (void);
  virtual uint32_t GetTxAvailable (void) const;
  virtual int Send (Ptr<Packet> p, uint32_t flags);
  virtual int SendTo (Ptr<Packet> p, uint32_t flags,
                      const Address &toAddress);
  virtual uint32_t GetRxAvailable (void) const;
  virtual Ptr<Packet> Recv (uint32_t maxSize, uint32_t flags);
  virtual Ptr<Packet> RecvFrom (uint32_t maxSize, uint32_t flags,
                                Address &fromAddress);
  virtual bool SetAllowBroadcast (bool allowBroadcast);
  virtual bool GetAllowBroadcast (void) const;

  void SetSockIndex (uint8_t sockIndex);

  uint32_t GetRcvBufSize (void) const;
  void SetRcvBufSize (uint32_t rcvBufSize);

  //
  void SendNotifyMessage (MapSockMsgType messageType);
  void SetMissMsgType (MissMsgType missMessageType);
  MissMsgType GetMissMsgType (void);

private:
  std::queue<Ptr<Packet> >GetDeliveryQueue (void);
  void Forward (Ptr<const Packet> packet, const Address &from);
  Ptr<LispOverIp> m_lisp;
  Address m_destAddres;
  uint8_t m_lispSockIndex;
  enum Socket::SocketErrno m_errno;   //!< Last error number.
  Ptr<Node> m_node;                 //!< Node
  bool m_shutdownSend;              //!< Flag to shutdown send capability.
  bool m_shutdownRecv;              //!< Flag to shutdown receive capability.
  bool m_connected;
  std::queue<Ptr<Packet> > m_deliveryQueue;     //!< Packet waiting to be processed.
  uint32_t m_rxAvailable;
  uint32_t m_rcvBufSize;
  MissMsgType m_lispMissMsgType;


};

} /* namespace ns3 */

#endif /* LISP_MAPPING_SOCKET_H_ */
