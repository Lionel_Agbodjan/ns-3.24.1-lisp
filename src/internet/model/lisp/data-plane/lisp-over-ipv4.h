/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 University of Liege
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lionel Agbodjan <lionel.agbodjan@gmail.com>
 */
#ifndef LISP_OVER_IPV4_H_
#define LISP_OVER_IPV4_H_

#include "ns3/ptr.h"
#include "ns3/node.h"
#include "ns3/packet.h"
#include "lisp-protocol.h"
#include "map-tables.h"
#include "lisp-over-ip.h"

namespace ns3
{

class Ipv4Header;
class Ipv4Route;
class MapTables;
class MapEntry;

class LispOverIpv4 : public LispOverIp {
public:

  static TypeId GetTypeId (void);
  LispOverIpv4 ();
  virtual
  ~LispOverIpv4 ();

  /**
   *
   */
  virtual void LispOutput (Ptr<Packet> packet, Ipv4Header const &innerHeader,
                           Ptr<const MapEntry> localMapping,
                           Ptr<const MapEntry> remoteMapping,
                           Ptr<Ipv4Route> lispRoute) = 0;

  /**
   *
   */
  virtual void LispInput (Ptr<Packet> packet, Ipv4Header const &outerHeader) = 0;

  // NB we give references of pointer because we want pointers to be modified
  virtual bool IsMapForEncapsulation (Ipv4Header const &innerHeader, Ptr<MapEntry> &srcMapEntry, Ptr<MapEntry> &destMapEntry, Ipv4Mask mask) const = 0;

  /**
   *
   */
  virtual bool NeedEncapsulation (Ipv4Header const &ipHeader, Ipv4Mask mask) = 0;

  /**
   *
   */
  virtual bool NeedDecapsulation (Ptr<const Packet> packet, Ipv4Header const &ipHeader) = 0;

  virtual Ptr<Packet> LispEncapsulate (Ptr<Packet> packet, uint16_t udpLength, uint16_t udpSrcPort) = 0;

  void RecordReceiveParams (Ptr<NetDevice> currentDevice, uint16_t protocol, NetDevice::PacketType packetType);

protected:
  Ptr<NetDevice> m_currentDevice;
  uint16_t m_ipProtocol;
  NetDevice::PacketType m_currentPacketType;
};

} /* namespace ns3 */

#endif /* LISP_OVER_IPV4_H_ */
