/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 University of Liege
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Lionel Agbodjan <lionel.agbodjan@gmail.com>
 */
#include "mapping-socket-msg.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv6-address.h"
#include "lisp-protocol.h"
#include "map-tables.h"

#include "ns3/log.h"
namespace ns3
{
NS_LOG_COMPONENT_DEFINE ("MappingSocketMsg");

NS_OBJECT_ENSURE_REGISTERED (MappingSocketMsgHeader);

NS_OBJECT_ENSURE_REGISTERED (MappingSocketMsg);

TypeId MappingSocketMsg::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::MappingSocketMsg")
        .SetGroupName ("Lisp")
        .AddConstructor<MappingSocketMsgHeader> ()
      ;
  return tid;
}

MappingSocketMsg::MappingSocketMsg ()
{

}

MappingSocketMsg::~MappingSocketMsg ()
{

}

void MappingSocketMsg::Print (std::ostream& os)
{

}

Address MappingSocketMsg::GetEidAddress (void)
{
  return Address ();
}
Ipv6Prefix MappingSocketMsg::GetIpv6Prefix (void)
{
  return Ipv6Prefix ();
}
Ipv4Mask MappingSocketMsg::GetMask (void)
{
  return Ipv4Mask ();
}

Ptr<Locators> MappingSocketMsg::GetLocators (void)
{
  return 0;
}

  TypeId MappingSocketMsgHeader::GetTypeId (void)
  {
    static TypeId tid = TypeId ("ns3::MappingSocketMsgHeader")
      .SetParent<Header> ()
      .SetGroupName ("Lisp")
      .AddConstructor<MappingSocketMsgHeader> ()
    ;
    return tid;
  }

  TypeId MappingSocketMsgHeader::GetInstanceTypeId (void) const
  {
    return GetTypeId ();
  }

  MappingSocketMsgHeader::MappingSocketMsgHeader ()
  {
    // TODO Auto-generated constructor stub

  }

  MappingSocketMsgHeader::~MappingSocketMsgHeader ()
  {
    // TODO Auto-generated destructor stub
  }

void MappingSocketMsgHeader::Print (std::ostream& os) const
{

}

uint32_t MappingSocketMsgHeader::GetSerializedSize (void) const
{
  return 0;
}

void MappingSocketMsgHeader::Serialize (Buffer::Iterator start) const
{

}

uint32_t MappingSocketMsgHeader::Deserialize (Buffer::Iterator start)
{
  return 0;
}
} /* namespace ns3 */
