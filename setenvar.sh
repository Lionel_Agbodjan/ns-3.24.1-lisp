#!/bin/bash

export NS3DIR="$PWD"
export NS3CONFIG="--enable-examples --enable-tests"
export NS3DEBUG="--build-profile=debug --out=build/debug"
export NS3OPT=="--build-profile=optimized --out=build/optimized"

function waff { CWD="$PWD" && cd $NS3DIR >/dev/null && ./waf --cwd="$CWD" $* && cd - >/dev/null ; }

